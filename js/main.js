const images = [
    'assets/img/photo1.jpg', 'assets/img/photo2.jpg', 'assets/img/photo3.jpg', 'assets/img/photo4.jpg', 'assets/img/photo5.jpg', 'assets/img/photo6.jpg', 'assets/img/photo7.jpg', 'assets/img/photo8.jpg', 'assets/img/photo9.jpg'
]

let i = 0;

function placeImage(x, y) {

    const nextImage = images[i]

    const img$$ = document.createElement('img')
    img$$.src = nextImage
    img$$.style.left = x + 'px'
    img$$.style.top = y + 'px'
    img$$.style.transform = 'translate(-50%, -50%) scale(0.075) rotate(' + (Math.random() * 20 - 10) + 'deg)'

    document.body.appendChild(img$$)

    i = i + 1

    if (i >= images.length) {
        i = 0;
    }
}
const hero$$ = document.getElementById('hero');
hero$$.addEventListener('mouseover', function (event) {
    event.preventDefault()
    placeImage(event.pageX, event.pageY)
})

hero$$.addEventListener('touchend', function (event) {
    event.preventDefault()
    placeImage(event.pageX, event.pageY)
})